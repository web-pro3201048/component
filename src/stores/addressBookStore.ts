import { ref } from 'vue'
import { defineStore } from 'pinia'

export const useAddressBookStore = defineStore('address_book', () => {
  const num = ref(0)
  
  function doInc() {
    num.value++
  }

  return { num, doInc}
})
