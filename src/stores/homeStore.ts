import { ref } from 'vue'
import { defineStore } from 'pinia'

export const useCounterStore = defineStore('counter', () => {
  const num = ref(0)
  
  function doInc() {
    num.value++
  }

  return { num, doInc}
})
